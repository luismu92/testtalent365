const URL = 'https://swapi.dev/api/films/';

const getAll = async () => {
  const fetchedData = await fetch(URL)
    .then(result => result.json())
    .then(data => {

      return data.results;

    })

  return fetchedData;
}

const getNewMainData = async (mainData) => {
  const newMainData = await mainData.map(el => ({
    name: el.title,
    planets: el.planets,
    people: el.characters,
    starships: el.starships
  }));
 
 return newMainData;
}


async function runAPI() {
  try {
    const mainData = await getAll();
    const newMainData = await getNewMainData(mainData);
   

    return console.log(newMainData);
  } catch (error) {
    return console.log(error);
  }
}

runAPI();